<?php

namespace Drupal\wayfinding\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that gets dispatched, when setting options get collected.
 *
 * @package Drupal\wayfinding\Event
 */
class SettingsOptionEvent extends Event {

  /**
   * The list of source types.
   *
   * @var array
   */
  protected array $sources;

  /**
   * The list of destination types.
   *
   * @var array
   */
  protected array $destinations;

  /**
   * SettingsOptionEvent constructor.
   *
   * @param array $sources
   *   The list of source types.
   * @param array $destinations
   *   The list of destination types.
   */
  public function __construct(array $sources, array $destinations) {
    $this->setSources($sources);
    $this->setDestinations($destinations);
  }

  /**
   * Get the list of source types.
   *
   * @return array
   *   The list of source types.
   */
  public function getSources(): array {
    return $this->sources;
  }

  /**
   * Set the list of source types.
   *
   * @param array $sources
   *   The list of source types.
   *
   * @return self
   *   This event.
   */
  public function setSources(array $sources): SettingsOptionEvent {
    $this->sources = $sources;
    asort($this->sources);
    return $this;
  }

  /**
   * Get the list of destination types.
   *
   * @return array
   *   The list of destination types.
   */
  public function getDestinations(): array {
    return $this->destinations;
  }

  /**
   * Set the list of destination types.
   *
   * @param array $destinations
   *   The list of destination types.
   *
   * @return self
   *   This event.
   */
  public function setDestinations(array $destinations): SettingsOptionEvent {
    $this->destinations = $destinations;
    asort($this->destinations);
    return $this;
  }

}
