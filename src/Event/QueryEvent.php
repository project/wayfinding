<?php

namespace Drupal\wayfinding\Event;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\wayfinding\Entity\Wayfinding;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event that gets dispatched, when a wayfinding destination gets queried.
 *
 * @package Drupal\wayfinding\Event
 */
class QueryEvent extends Event {

  /**
   * The destination entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected ContentEntityInterface $destination;

  /**
   * The list of source entities.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface[]
   */
  protected array $sources = [];

  /**
   * SettingsOptionEvent constructor.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $destination
   *   The destination entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $sources
   *   The list of source entities.
   */
  public function __construct(ContentEntityInterface $destination, array $sources) {
    $this->destination = $destination;
    $this->sources = $sources;
  }

  /**
   * Gets the destination entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The destination entity.
   */
  public function getDestination(): ContentEntityInterface {
    return $this->destination;
  }

  /**
   * Get the list of source entities.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The list of source entities.
   */
  public function getSources(): array {
    return $this->sources;
  }

  /**
   * Set the list of source entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $sources
   *   The list of source entities.
   *
   * @return self
   *   This event.
   */
  public function setSources(array $sources): QueryEvent {
    $this->sources = [];
    foreach ($sources as $source) {
      if ($source->get('wayfinding')->getValue()) {
        /** @var \Drupal\wayfinding\Entity\Wayfinding|null $wayfinding */
        $wayfinding = Wayfinding::load($source->get('wayfinding')->getValue()[0]['target_id']);
        if ($wayfinding !== NULL && $wayfinding->isEnabled()) {
          $this->sources[] = $source;
        }
      }
    }
    return $this;
  }

}
