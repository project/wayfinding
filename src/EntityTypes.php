<?php

namespace Drupal\wayfinding;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides entity type services.
 */
class EntityTypes {

  public const EXCLUDED = [
    'aggregator_feed',
    'aggregator_item',
    'block_content',
    'comment',
    'contact_message',
    'content_moderation_state',
    'crop',
    'digital_signage_content_setting',
    'digital_signage_device',
    'digital_signage_schedule',
    'digsig_computed_content',
    'entity_embed_fake_entity',
    'helpdesk_issue',
    'menu_link_content',
    'onesignal_device',
    'paragraph',
    'paragraphs_library_item',
    'path_alias',
    'redirect',
    'danse_event',
    'danse_notification',
    'danse_notification_action',
    'shortcut',
    'smart_date_override',
    'smart_date_rule',
    'wayfinding',
    'webform_submission',
    'workspace',
  ];


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Renderer constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Gets a list of all entity types.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   List of all entity types keyed by their ID.
   */
  public function all(): array {
    $result = [];
    foreach ($this->entityTypeManager->getDefinitions() as $id => $definition) {
      if ($definition instanceof ContentEntityTypeInterface) {
        $result[$id] = $definition;
      }
    }
    return $result;
  }

  /**
   * Gets a list of all entity types that are enabled for wayfinding.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   List of all entity types, that are enabled for wayfinding, keyed by
   *   their ID.
   */
  public function allEnabled(): array {
    $result = [];
    $all = $this->all();
    foreach ($this->allEnabledIds() as $id) {
      $result[$id] = $all[$id];
    }
    return $result;
  }

  /**
   * Gets a list of all entity type keys.
   *
   * @return string[]
   *   The list of all entity type keys.
   */
  public function allIds(): array {
    return array_keys($this->all());
  }

  /**
   * Gets a list of all entity type keys that are enabled for wayfinding.
   *
   * @return string[]
   *   The list of all entity type keys, that are enabled for wayfinding.
   */
  public function allEnabledIds(): array {
    return array_diff($this->allIds(), $this->allDisabledIds());
  }

  /**
   * Gets a list of all entity type keys that are disabled for wayfinding.
   *
   * @return string[]
   *   The list of all entity type keys, that are disabled for wayfinding.
   */
  public function allDisabledIds(): array {
    // @todo Allow other modules to alter this list.
    return self::EXCLUDED;
  }

}
