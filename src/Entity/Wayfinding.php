<?php

namespace Drupal\wayfinding\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\wayfinding\WayfindingInterface;

/**
 * Defines the wayfinding entity class.
 *
 * @ContentEntityType(
 *   id = "wayfinding",
 *   label = @Translation("Wayfinding"),
 *   label_collection = @Translation("Wayfindings"),
 *   handlers = {
 *     "views_data" = "Drupal\wayfinding\ViewsData"
 *   },
 *   base_table = "wayfinding",
 *   admin_permission = "administer wayfinding",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class Wayfinding extends ContentEntityBase implements WayfindingInterface {

  /**
   * {@inheritdoc}
   */
  public function getReverseEntity(): ?array {
    $item = $this->get('parent_entity');
    if (!$item->isEmpty()) {
      $reverse_entity = $item[0]->getValue();
      if (!empty($reverse_entity['target_id'])) {
        return $reverse_entity;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setReverseEntity($entity): WayfindingInterface {
    $this->set('parent_entity', $entity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setReverseEntityTypeAndBundle($type_and_bundle): WayfindingInterface {
    $this->set('parent_entity_type_bundle', $type_and_bundle);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): WayfindingInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isAutoLabel(): bool {
    return (bool) $this->get('auto_label')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    return $this->get('label')->value ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label): WayfindingInterface {
    $this->set('label', $label);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Publish'))
      ->setDescription(t('Enable to publish this content to wayfinding.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
    $fields['auto_label'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Automatic label'))
      ->setDescription(t('Use the label from the parent when enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);
    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE);
    $fields['geolocation'] = BaseFieldDefinition::create('geolocation')
      ->setLabel(t('Geo location'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE);
    $fields['svgid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('SVG ID'))
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'weight' => 4,
      ]);
    $fields['parent_entity'] = BaseFieldDefinition::create('dynamic_entity_reference')
      ->setRevisionable(FALSE)
      ->setLabel(t('Parent entity'))
      ->setDescription(t('The entity of the content which contains these settings.'));
    $fields['parent_entity_type_bundle'] = BaseFieldDefinition::create('string')
      ->setRevisionable(FALSE)
      ->setLabel(t('Parent entity type and bundle'))
      ->setDescription(t('The type and bundle of the entity of the content which contains these settings.'));

    return $fields;
  }

}
