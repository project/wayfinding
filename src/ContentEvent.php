<?php

namespace Drupal\wayfinding;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\wayfinding\Entity\Wayfinding;

/**
 * Content event service.
 */
class ContentEvent {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Load the wayfinding settings for an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\wayfinding\WayfindingInterface|false|null
   *   FALSE, if the entity is not relevant for wayfinding. The settings
   *   otherwise or NULL, if the entity does not yet have settings associated.
   */
  private function loadSettings(EntityInterface $entity): WayfindingInterface|bool|null {
    if (!($entity instanceof ContentEntityInterface)) {
      // Only deal with content entities.
      return FALSE;
    }

    if (!$entity->hasField('wayfinding')) {
      // Only deal with entities that have the wayfinding field.
      return FALSE;
    }

    // Load the settings entity.
    $target = $entity->get('wayfinding')->getValue();
    if (!isset($target[0]['target_id'])) {
      // Might be missing in some circumstances.
      return FALSE;
    }
    /** @var \Drupal\wayfinding\WayfindingInterface|null $settings */
    $settings = Wayfinding::load($target[0]['target_id']);
    return $settings;
  }

  /**
   * Create a settings entity if none exists.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity that gets presaved.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function presave(EntityInterface $entity): void {
    $settings = $this->loadSettings($entity);
    if ($settings === NULL) {
      $settings = Wayfinding::create();
      $settings->save();
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity->set('wayfinding', $settings->id());
    }
  }

  /**
   * Update the settings of an updated entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be updated.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function update(EntityInterface $entity): void {
    $settings = $this->loadSettings($entity);
    if ($settings === FALSE) {
      return;
    }
    if ($settings === NULL) {
      // This shouldn't ever happen as the presave hook should have made sure,
      // that we always have a settings entity.
      // @todo Write an exception to watchdog/logger.
      return;
    }
    $changed = FALSE;

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    // Store reverse reference.
    if (!$settings->getReverseEntity()) {
      $settings
        ->setReverseEntity($entity)
        ->setReverseEntityTypeAndBundle(implode(' ', [$entity->getEntityTypeId(), $entity->bundle()]));
      $changed = TRUE;
    }

    // Disable wayfinding ifreverse entity is disabled.
    if ($entity->hasField('status') && !$entity->get('status')->value && $settings->isEnabled()) {
      $settings->setStatus(FALSE);
      $changed = TRUE;
    }

    if ((empty($settings->getLabel()) || $settings->isAutoLabel()) && $settings->getLabel() !== $entity->label()) {
      $settings->setLabel($entity->label());
      $changed = TRUE;
    }

    if ($changed) {
      $settings->save();
    }
  }

}
