<?php

namespace Drupal\wayfinding;

/**
 * Contains all events thrown by the wayfinding module.
 */
final class WayfindingEvents {

  const SETTINGSOPTIONS = 'settings.options';

  const QUERY = 'query';

  const LIBRARIES = 'libraries';

}
