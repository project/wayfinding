<?php

namespace Drupal\wayfinding;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a wayfinding entity type.
 */
interface WayfindingInterface extends ContentEntityInterface {

  /**
   * Get the parent entity.
   *
   * @return array|null
   *   An array with the entity type and ID of the parent, if that exists,
   *   NULL otherwise.
   */
  public function getReverseEntity(): ?array;

  /**
   * Sets the parent entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The parent entity.
   *
   * @return self
   *   This wayfinding entity.
   */
  public function setReverseEntity(ContentEntityInterface $entity): WayfindingInterface;

  /**
   * Sets the parent entity type and bundle.
   *
   * @param string $type_and_bundle
   *   The type and bundle of the parent entity.
   *
   * @return self
   *   This wayfinding entity.
   */
  public function setReverseEntityTypeAndBundle(string $type_and_bundle): WayfindingInterface;

  /**
   * Returns the wayfinding status.
   *
   * @return bool
   *   TRUE if wayfinding is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the wayfinding status.
   *
   * @param bool $status
   *   TRUE to enable wayfinding, FALSE to disable.
   *
   * @return \Drupal\wayfinding\WayfindingInterface
   *   The called wayfinding entity.
   */
  public function setStatus(bool $status): WayfindingInterface;

  /**
   * Returns the wayfinding auto-label mode.
   *
   * @return bool
   *   TRUE if auto-label is enabled, FALSE otherwise.
   */
  public function isAutoLabel(): bool;

  /**
   * Returns the wayfinding label.
   *
   * @return string
   *   The wayfinding label.
   */
  public function getLabel(): string;

  /**
   * Sets the wayfinding label.
   *
   * @param string $label
   *   The wayfinding label.
   *
   * @return self
   *   This wayfinding entity.
   */
  public function setLabel(string $label): WayfindingInterface;

}
