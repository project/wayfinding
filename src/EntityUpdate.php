<?php

namespace Drupal\wayfinding;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Entity update service.
 */
class EntityUpdate {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected EntityDefinitionUpdateManagerInterface $updateManager;

  /**
   * The entity types services.
   *
   * @var \Drupal\wayfinding\EntityTypes
   */
  protected EntityTypes $entityTypes;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   * @param \Drupal\wayfinding\EntityTypes $entity_types
   *   The wayfinding entity types service.
   */
  public function __construct(EntityTypeManager $entity_type_manager, EntityDefinitionUpdateManagerInterface $update_manager, EntityTypes $entity_types) {
    $this->entityTypeManager = $entity_type_manager;
    $this->updateManager = $update_manager;
    $this->entityTypes = $entity_types;
  }

  /**
   * Gets the base field definitions.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   The base field definitions.
   */
  public function fieldDefinition(): BaseFieldDefinition {
    return BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Wayfinding'))
      ->setSetting('target_type', 'wayfinding')
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'inline_entity_form_simple',
        'settings' => [
          'form_mode' => 'default',
          'label_singular' => '',
          'label_plural' => '',
          'collapsible' => TRUE,
          'collapsed' => TRUE,
          'override_labels' => FALSE,

        ],
        'weight' => 99,
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Method description.
   */
  public function updateExistingEntityTypes(): void {
    $field_definition = $this->fieldDefinition();
    /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (($definition instanceof ContentEntityTypeInterface) && !in_array($definition->id(), $this->entityTypes->allDisabledIds(), TRUE)) {
        $this->updateManager->installFieldStorageDefinition('wayfinding', $definition->id(), $definition->getProvider(), $field_definition);
      }
    }
  }

}
