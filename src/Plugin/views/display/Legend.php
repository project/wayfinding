<?php

namespace Drupal\wayfinding\Plugin\views\display;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\views\Plugin\views\display\Attachment;
use Drupal\wayfinding\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The plugin that handles a wayfinding legend.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "wayfinding_legend",
 *   title = @Translation("Wayfinding Legend"),
 *   help = @Translation("Display the view as a wayfinding legend."),
 *   theme = "views_view",
 *   register_theme = FALSE,
 *   uses_hook_block = FALSE,
 *   contextual_links_locations = {""},
 *   admin = @Translation("Wayfinding Legend")
 * )
 */
class Legend extends Attachment {

  /**
   * The query services.
   *
   * @var \Drupal\wayfinding\Query
   */
  protected Query $query;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): Legend {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->query = $container->get('wayfinding.query');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->renderer = $container->get('renderer');
    $instance->config = $container->get('config.factory')->get('wayfinding.settings');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): array|null {
    $output = parent::execute();
    $this->processRows($output['#rows']);
    return $output;
  }

  /**
   * Helper function to process the rows of the legend.
   *
   * @param array $rows
   *   The legend rows.
   */
  private function processRows(array &$rows): void {
    foreach (Element::children($rows) as $id) {
      $outerRow = &$rows[$id];
      if (!empty($outerRow['#rows'])) {
        foreach ($outerRow['#rows'] as &$row) {
          if (empty($row['#row'])) {
            $this->processRows($row);
          }
          else {
            $entity = $row['#row']->_entity->getReverseEntity();
            $srcid = 'wayfinding-src-id-' . $entity['target_type'] . '-' . $entity['target_id'];
            $destids = [];
            foreach ($this->query->getDestinationsForId($entity) as $destination) {
              $destids[] = 'wayfinding-id-' . $destination['target_type'] . '-' . $destination['target_id'];
            }
            if (empty($destids)) {
              // Assume the source is also the destination.
              $destids[] = 'wayfinding-id-' . $entity['target_type'] . '-' . $entity['target_id'];
            }
            $row['#prefix'] = '<div class="source" id="' . $srcid . '" destinations="' . implode(',', $destids) . '">';
            $row['#suffix'] = '</div>';
          }
        }
      }
    }
  }

}
