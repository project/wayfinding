<?php

namespace Drupal\wayfinding\Plugin\views\style;

use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\wayfinding\Query;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Wayfinding plugin to render destinations and embed one or many legends.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "wayfinding_master",
 *   title = @Translation("Wayfinding master"),
 *   help = @Translation("Displays wayfinding destinations and embeds one or many legends."),
 *   theme = "views_view_wayfinding_master",
 *   display_types = {"normal"}
 * )
 */
class Master extends StylePluginBase {

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * The query services.
   *
   * @var \Drupal\wayfinding\Query
   */
  protected Query $query;

  /**
   * The list of destinations.
   *
   * @var array
   */
  protected static array $destinations = [];

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): Master {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->query = $container->get('wayfinding.query');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($result): void {
    /** @var \Drupal\views\ResultRow $row */
    foreach ($result as $row) {
      /** @var \Drupal\wayfinding\Entity\Wayfinding $wayfinding */
      $wayfinding = $row->_entity;
      $entity = $wayfinding->getReverseEntity();
      self::$destinations[] = $entity;
      $row->wayfinding_id = 'wayfinding-id-' . $entity['target_type'] . '-' . $entity['target_id'];
      $row->img = $this->query->getRenderedMedia(0, $entity['target_type'], $entity['target_id']);
      $row->svgid = $wayfinding->get('svgid')->value ?? [];
      $row->geolocation = $wayfinding->get('geolocation')->getValue()[0];
    }
  }

  /**
   * Gets the list of destinations.
   *
   * @return array
   *   The list of destinations.
   */
  public static function getDestinations(): array {
    return self::$destinations;
  }

}
