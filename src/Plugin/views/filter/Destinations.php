<?php

namespace Drupal\wayfinding\Plugin\views\filter;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\query\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter handler for wayfinding destinations.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("wayfinding_destinations")
 */
class Destinations extends FilterPluginBase {

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): Destinations {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->config = $container->get('config.factory')->get('wayfinding.settings');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary(): void {}

  /**
   * {@inheritdoc}
   */
  protected function operatorForm(&$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function canExpose(): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    if (!($this->query instanceof Sql)) {
      return;
    }
    $table = $this->ensureMyTable();
    $types = [];
    foreach ($this->config->get('types')['destinations'] as $key => $value) {
      if (!empty($value)) {
        $types[] = $key;
      }

    }
    $types = implode("','", $types);
    $snippet = "$table.parent_entity_type_bundle IN ('$types')";
    $this->query->addWhereExpression($this->options['group'], $snippet);
  }

}
