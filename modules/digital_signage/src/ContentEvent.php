<?php

namespace Drupal\wayfinding_digital_signage;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\digital_signage_computed_content\ComputedContentInterface;
use Drupal\digital_signage_computed_content\Entity\ComputedContent;
use Drupal\digital_signage_framework\DeviceInterface;
use Drupal\digital_signage_framework\Entity\ContentSetting;

/**
 * Content event service.
 */
class ContentEvent {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Helper function to safely save an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE, if the entity was saved successfully, FALSE otherwise.
   */
  protected function saveEntity(EntityInterface $entity): bool {
    try {
      $entity->save();
      return TRUE;
    }
    catch (EntityStorageException) {
      // @todo Log this exception.
    }
    return FALSE;
  }

  /**
   * Helper function to create a title.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return string
   *   The computed title.
   */
  protected function getComputedContentTitle(DeviceInterface $device): string {
    return 'Wayfinding for device ' . $device->label();
  }

  /**
   * Helper function to generate a wrapper entity.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return \Drupal\digital_signage_computed_content\ComputedContentInterface
   *   The generated entity.
   */
  protected function getComputedContentEntity(DeviceInterface $device): ComputedContentInterface {
    try {
      /** @var \Drupal\digital_signage_framework\ContentSettingInterface[] $entities */
      $entities = $this->entityTypeManager->getStorage('digital_signage_content_setting')
        ->loadByProperties([
          'parent_entity__target_type' => 'digsig_computed_content',
          'parent_entity_bundle' => 'wayfinding',
          'devices' => $device->id(),
        ]);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      // @todo Log this exception.
    }
    if (empty($entities)) {
      /** @var \Drupal\digital_signage_framework\ContentSettingInterface $settings */
      $settings = ContentSetting::create(['devices' => [$device]]);
      $this->saveEntity($settings);
    }
    else {
      $settings = reset($entities);
    }

    $content = NULL;
    if ($reverse_entity = $settings->getReverseEntity()) {
      /** @var \Drupal\digital_signage_computed_content\ComputedContentInterface $content */
      $content = ComputedContent::load($reverse_entity['target_id']);
    }
    if ($content === NULL) {
      /** @var \Drupal\digital_signage_computed_content\ComputedContentInterface $content */
      $content = ComputedContent::create([
        'bundle' => 'wayfinding',
        'digital_signage' => $settings,
        'title' => $this->getComputedContentTitle($device),
      ]);
      $this->saveEntity($content);
    }
    return $content;
  }

  /**
   * Create an entity for a newly inserted device.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The device.
   */
  public function insert(EntityInterface $entity): void {
    if (!($entity instanceof DeviceInterface)) {
      // Only deal with digital signage device entities.
      return;
    }
    $this->getComputedContentEntity($entity);
  }

  /**
   * Update an entity for an existing device.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The device.
   */
  public function update(EntityInterface $entity): void {
    if (!($entity instanceof DeviceInterface)) {
      // Only deal with digital signage device entities.
      return;
    }
    $content = $this->getComputedContentEntity($entity);
    $title = $this->getComputedContentTitle($entity);
    if ($title !== $content->getTitle()) {
      $content->setTitle($title);
      $this->saveEntity($content);
    }
  }

  /**
   * Delete an entity for a device which got deleted.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The device.
   */
  public function delete(EntityInterface $entity): void {
    if (!($entity instanceof DeviceInterface)) {
      // Only deal with digital signage device entities.
      return;
    }
    $content = $this->getComputedContentEntity($entity);
    try {
      $content->delete();
    }
    catch (EntityStorageException) {
      // @todo Log this exception.
    }
  }

}
