<?php

namespace Drupal\wayfinding_digital_signage;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\digital_signage_computed_content\ComputedContentInterface;
use Drupal\digital_signage_computed_content\RenderInterface;
use Drupal\digital_signage_framework\Entity\ContentSetting;
use Drupal\digital_signage_framework\Entity\Device;
use Drupal\wayfinding\Query;

/**
 * Provides render services for digital signage.
 *
 * @package Drupal\wayfinding_digital_signage
 */
class Render implements RenderInterface {

  use StringTranslationTrait;

  /**
   * The query services.
   *
   * @var \Drupal\wayfinding\Query
   */
  protected Query $query;

  /**
   * The entity updates services.
   *
   * @var \Drupal\wayfinding_digital_signage\EntityUpdate
   */
  protected EntityUpdate $entityUpdate;

  /**
   * Render constructor.
   *
   * @param \Drupal\wayfinding\Query $query
   *   The query services.
   * @param \Drupal\wayfinding_digital_signage\EntityUpdate $entity_update
   *   The entity update services.
   */
  public function __construct(Query $query, EntityUpdate $entity_update) {
    $this->query = $query;
    $this->entityUpdate = $entity_update;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarkup(ComputedContentInterface $entity): array {
    $settingsTarget = $entity->get('digital_signage')->getValue();
    if (isset($settingsTarget[0]['target_id'])) {
      /** @var \Drupal\digital_signage_framework\ContentSettingInterface|null $settings */
      $settings = ContentSetting::load($settingsTarget[0]['target_id']);
      if ($settings && ($device_ids = $settings->getDeviceIds())) {
        $device_id = reset($device_ids);
        /** @var \Drupal\digital_signage_framework\DeviceInterface|null $device */
        $device = Device::load($device_id);
        if ($device) {
          $position = $this->entityUpdate->getGeolocation($device);
          return $this->query->build(
            TRUE,
            $this->entityUpdate->getPerspective($device),
            $position['lat'],
            $position['lng'],
            $device->id(),
            $device->extId()
          );
        }
      }
    }
    return [
      '#markup' => $this->t('Something went wrong loading wayfinding information.'),
    ];
  }

}
