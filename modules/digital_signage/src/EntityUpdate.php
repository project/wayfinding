<?php

namespace Drupal\wayfinding_digital_signage;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\digital_signage_framework\DeviceInterface;

/**
 * Entity update service.
 */
class EntityUpdate {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface
   */
  protected EntityDefinitionUpdateManagerInterface $updateManager;

  /**
   * The content event services.
   *
   * @var \Drupal\wayfinding_digital_signage\ContentEvent
   */
  protected ContentEvent $contentEvent;

  /**
   * Constructs an Entity update service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   * @param \Drupal\wayfinding_digital_signage\ContentEvent $content_event
   *   The content event services.
   */
  public function __construct(EntityTypeManager $entity_type_manager, EntityDefinitionUpdateManagerInterface $update_manager, ContentEvent $content_event) {
    $this->entityTypeManager = $entity_type_manager;
    $this->updateManager = $update_manager;
    $this->contentEvent = $content_event;
  }

  /**
   * Provides the required base field definitions.
   *
   * @return array
   *   The base field definitions.
   */
  public function fieldDefinition(): array {
    $fields = [];

    $fields['pin_location_x'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('PIN location x'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'settings' => [
          'size' => 'normal',
          'unsigned' => TRUE,
          'min' => 0,
        ],
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 13,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['pin_location_y'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('PIN location y'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'settings' => [
          'size' => 'normal',
          'unsigned' => TRUE,
          'min' => 0,
        ],
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['geolocation'] = BaseFieldDefinition::create('geolocation')
      ->setLabel(t('Geo location'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'weight' => 8,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 11,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['perspective'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Perspective'))
      ->setRequired(FALSE)
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'settings' => [
          'size' => 'normal',
          'unsigned' => TRUE,
          'min' => 0,
          'max' => 360,
        ],
        'weight' => 9,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'weight' => 12,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    $fields['wayfinding_link'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Wayfinding link'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Show wayfinding link')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Method description.
   */
  public function updateExistingEntityTypes(): void {
    /** @var \Drupal\Core\Entity\EntityTypeInterface $definition */
    foreach ($this->entityTypeManager->getDefinitions() as $definition) {
      if (($definition instanceof ContentEntityTypeInterface) && $definition->id() === 'digital_signage_device') {
        foreach ($this->fieldDefinition() as $field_name => $field_definition) {
          $this->updateManager->installFieldStorageDefinition($field_name, $definition->id(), $definition->getProvider(), $field_definition);
        }
      }
    }
  }

  /**
   * Method description.
   */
  public function createComputedContentForAllDevices(): void {
    try {
      /** @var \Drupal\digital_signage_framework\DeviceInterface $device */
      foreach ($this->entityTypeManager->getStorage('digital_signage_device')->loadMultiple() as $device) {
        $this->contentEvent->insert($device);
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      // @todo Log this exception.
    }
  }

  /**
   * Get perspective of device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return float
   *   The device perspective.
   */
  public function getPerspective(DeviceInterface $device): float {
    return $device->get('perspective')->value ?? 0.00;
  }

  /**
   * Get geolocation of device.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $device
   *   The device.
   *
   * @return array
   *   The geolocation.
   */
  public function getGeolocation(DeviceInterface $device): array {
    $location = $device->get('geolocation');
    if (!$location->isEmpty()) {
      $location = $location->getValue()[0];
    }
    else {
      $location = [
        'lat' => 0,
        'lng' => 0,
      ];
    }
    return $location;
  }

}
