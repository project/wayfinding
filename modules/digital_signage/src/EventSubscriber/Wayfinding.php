<?php

namespace Drupal\wayfinding_digital_signage\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\digital_signage_framework\DigitalSignageFrameworkEvents;
use Drupal\digital_signage_framework\Event\Libraries;
use Drupal\digital_signage_framework\Event\Overlays;
use Drupal\wayfinding\Query;
use Drupal\wayfinding_digital_signage\EntityUpdate;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Way finding event subscriber.
 */
class Wayfinding implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The query services.
   *
   * @var \Drupal\wayfinding\Query
   */
  protected Query $query;

  /**
   * The entity update services.
   *
   * @var \Drupal\wayfinding_digital_signage\EntityUpdate
   */
  protected EntityUpdate $entityUpdate;

  /**
   * Wayfinding constructor.
   *
   * @param \Drupal\wayfinding\Query $query
   *   The query services.
   * @param \Drupal\wayfinding_digital_signage\EntityUpdate $entity_update
   *   The entity update services.
   */
  public function __construct(Query $query, EntityUpdate $entity_update) {
    $this->query = $query;
    $this->entityUpdate = $entity_update;
  }

  /**
   * Gets dispatched when libraries are selected.
   *
   * @param \Drupal\digital_signage_framework\Event\Libraries $event
   *   The libraries event.
   */
  public function onLibraries(Libraries $event): void {
    $event->addLibrary('wayfinding/general');
    $event->addLibrary('wayfinding/popup');
    $position = $this->entityUpdate->getGeolocation($event->getDevice());
    $event->addSettings('wayfinding', $this->query->getSettings(
      $this->entityUpdate->getPerspective($event->getDevice()),
      $position['lat'],
      $position['lng']));
  }

  /**
   * Gets dispatched when overlays are selected.
   *
   * @param \Drupal\digital_signage_framework\Event\Overlays $event
   *   The overlay event.
   */
  public function onOverlays(Overlays $event): void {
    if (!$event->getDevice()->get('wayfinding_link')->value) {
      return;
    }
    $url = Url::fromRoute('wayfinding_digital_signage.device.extid', ['id' => $event->getDevice()->extId()], [
      'absolute' => TRUE,
    ]);
    $link = '<div class="wayfinding-link"><a href="' . $url->toString() . '">' . $this->t('Wayfinding') . '</a></div>';
    $event->addOverlay('wayfinding-link', $this->t('Wayfinding link'), $link, []);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      DigitalSignageFrameworkEvents::LIBRARIES => ['onLibraries'],
      DigitalSignageFrameworkEvents::OVERLAYS => ['onOverlays'],
    ];
  }

}
