<?php

namespace Drupal\wayfinding_digital_signage\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Render\AttachmentsInterface;
use Drupal\digital_signage_framework\DeviceInterface;
use Drupal\wayfinding\Controller\Wayfinding;
use Drupal\wayfinding_digital_signage\EntityUpdate;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Device controller.
 */
class Device extends Wayfinding {

  /**
   * The entity update service.
   *
   * @var \Drupal\wayfinding_digital_signage\EntityUpdate
   */
  protected EntityUpdate $entityUpdate;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): Device {
    /** @var \Drupal\wayfinding_digital_signage\Controller\Device $instance */
    $instance = parent::create($container);
    $instance->entityUpdate = $container->get('wayfinding_digital_signage.entity_update');
    return $instance;
  }

  /**
   * Tbd.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $digital_signage_device
   *   Tbd.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Tbd.
   */
  public function accessDevice(DeviceInterface $digital_signage_device): AccessResultInterface {
    return $digital_signage_device->isEnabled() ?
      AccessResult::allowed() :
      AccessResult::forbidden();
  }

  /**
   * Tbd.
   *
   * @param string $id
   *   Tbd.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Tbd.
   */
  public function accessExtDevice(string $id): AccessResultInterface {
    try {
      /** @var \Drupal\digital_signage_framework\DeviceInterface[] $devices */
      $devices = $this->entityTypeManager->getStorage('digital_signage_device')
        ->loadByProperties([
          'extid' => $id,
        ]);
      if (empty($devices)) {
        return AccessResult::forbidden('Unknown external ID');
      }
      if (count($devices) > 1) {
        return AccessResult::forbidden('Ambiguous external ID');
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      return AccessResult::forbidden();
    }
    $digital_signage_device = reset($devices);
    return $this->accessDevice($digital_signage_device);
  }

  /**
   * Tbd.
   *
   * @param \Drupal\digital_signage_framework\DeviceInterface $digital_signage_device
   *   Tbd.
   *
   * @return \Drupal\Core\Render\AttachmentsInterface
   *   Tbd.
   */
  public function requestDevice(DeviceInterface $digital_signage_device): AttachmentsInterface {
    $position = $this->entityUpdate->getGeolocation($digital_signage_device);
    return $this->deliver(
      $this->entityUpdate->getPerspective($digital_signage_device),
      $position['lat'],
      $position['lng'],
      $digital_signage_device->id(),
      $digital_signage_device->extId()
    );
  }

  /**
   * Tbd.
   *
   * @param string $id
   *   Tbd.
   *
   * @return \Drupal\Core\Render\AttachmentsInterface
   *   Tbd.
   */
  public function requestExtDevice(string $id): AttachmentsInterface {
    /** @var \Drupal\digital_signage_framework\DeviceInterface[] $devices */
    $devices = [];
    try {
      /** @var \Drupal\digital_signage_framework\DeviceInterface[] $devices */
      $devices = $this->entityTypeManager->getStorage('digital_signage_device')
        ->loadByProperties([
          'extid' => $id,
        ]);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException) {
      // We can ignore this because it has been validated in access callback.
    }
    $digital_signage_device = reset($devices);
    return $this->requestDevice($digital_signage_device);
  }

}
