(function ($, Drupal, drupalSettings) {
  Drupal.wayfinding = Drupal.wayfinding || {};

  Drupal.behaviors.wayfinding = {
    attach() {
      $(
        '.popup-content-background:not(.wayfinding-processed), .popup-content-close:not(.wayfinding-processed)',
      )
        .addClass('wayfinding-processed')
        .on('click', function () {
          Drupal.wayfinding.popupClose();
          Drupal.wayfinding.setResetTimer(true);
        });
      $('.wayfinding-tab:not(.wayfinding-processed)')
        .addClass('wayfinding-processed')
        .on('click', function () {
          Drupal.wayfinding.reset();
          const id = $(this).attr('data-id');
          $(this)
            .closest('.legend-wrapper')
            .find('.active')
            .removeClass('active');
          $(this).addClass('active');
          $(`.wayfinding-wrapper .${id}`).addClass('active');
          Drupal.wayfinding.setResetTimer(true);
        });
      $('.legend .source:not(.wayfinding-processed)')
        .addClass('wayfinding-processed')
        .on('click', function () {
          if ($(this).hasClass('active')) {
            return false;
          }
          Drupal.wayfinding.reset();
          Drupal.wayfinding.popupSource($(this).attr('id'));
          Drupal.wayfinding.setResetTimer(true);
          const destinationsCollection = $(this).attr('destinations');
          if (destinationsCollection !== '') {
            const destinations = destinationsCollection.split(',');
            const widgetdata = {
              position: drupalSettings.wayfinding.position,
              destinations: [],
              routeAsLink: Drupal.digital_signage_timer === undefined,
            };
            $(this).addClass('active');
            for (const i in destinations) {
              $(`#${destinations[i]}`).each(function () {
                const svgid = $(this).attr('data-svgid');
                if (svgid !== '') {
                  $(`#wayfinding .background svg #${svgid}`).addClass(
                    'wayfinding-show',
                  );
                }
                $(this).addClass('show');
                widgetdata.destinations.push({
                  id: $(this).attr('id'),
                  lat: $(this).attr('data-geolocation-lat'),
                  lng: $(this).attr('data-geolocation-lng'),
                });
              });
            }
            const url = `${
              drupalSettings.wayfinding.widgeturl
            }?data=${JSON.stringify(widgetdata)}`;
            drupalSettings.ajaxTrustedUrl[url] = 1;
            Drupal.ajax({
              url,
              error(e) {
                console.log(e);
              },
            }).execute();
          }
          Drupal.wayfinding.setResetTimer(false);
          Drupal.wayfinding.calculateContainerSize();
          return false;
        });
      $('#wayfinding:not(.wayfinding-processed)')
        .addClass('wayfinding-processed')
        .each(function () {
          Drupal.wayfinding.calculateContainerSize();
          Drupal.wayfinding.setPin();
          Drupal.wayfinding.setResetTimer(false);
        });
      $(window).on('resize', function () {
        Drupal.wayfinding.calculateContainerSize();
        Drupal.wayfinding.setPin();
        Drupal.wayfinding.setResetTimer(false);
      });
    },
  };

  Drupal.wayfinding.calculateContainerSize = function () {
    const wheight = parseInt($(window).height());
    const wwidth = parseInt($(window).width());
    $('.wayfinding-wrapper.orientation-unknown').each(function () {
      let orientationClass = 'orientation-landscape';
      if (wheight > wwidth) {
        orientationClass = 'orientation-portrait';
      }
      $(this)
        .removeClass('orientation-landscape')
        .removeClass('orientation-portrait')
        .addClass(orientationClass);
    });
    if (wheight > wwidth) {
      let top = 0;
      const popup = $('#wayfinding-popup');
      if (popup && popup.first() && popup.first().position()) {
        top = parseInt(popup.first().position().top);
      }
      const height = parseInt($('#wayfinding .images-wrapper').height());
      const remaining_height = wheight - top - height;
      if (remaining_height > 0) {
        $('#wayfinding .legend-wrapper').height(remaining_height);
      } else {
        window.setTimeout(function () {
          Drupal.wayfinding.calculateContainerSize();
        }, 2000);
      }
    }
  };

  Drupal.wayfinding.setPin = function () {
    const pin = $(
      '.wayfinding-wrapper .images .background .pin svg, .wayfinding-wrapper .images .background .pin img',
    );
    if (!drupalSettings.wayfinding.pinDynamicPosition) {
      $(pin).hide();
      return;
    }
    if (
      drupalSettings.wayfinding.position.lat === 0 &&
      drupalSettings.wayfinding.position.lng === 0
    ) {
      $(pin).hide();
      return;
    }
    if (
      drupalSettings.wayfinding.topleft.lat === 0 &&
      drupalSettings.wayfinding.topleft.lng === 0
    ) {
      $(pin).hide();
      return;
    }
    const bgimg = $('.wayfinding-wrapper .images .background');
    const rotation = parseFloat(drupalSettings.wayfinding.perspective);
    const p = {
      latitude: parseFloat(drupalSettings.wayfinding.position.lat) * 10000000,
      longitude: parseFloat(drupalSettings.wayfinding.position.lng) * 10000000,
    };
    const a = {
      latitude: parseFloat(drupalSettings.wayfinding.topleft.lat) * 10000000,
      longitude: parseFloat(drupalSettings.wayfinding.topleft.lng) * 10000000,
    };
    const topLeft = {
      latitude:
        parseFloat(drupalSettings.wayfinding.location.topleft.lat) * 10000000,
      longitude:
        parseFloat(drupalSettings.wayfinding.location.topleft.lng) * 10000000,
    };
    const bottomRight = {
      latitude:
        parseFloat(drupalSettings.wayfinding.location.bottomright.lat) *
        10000000,
      longitude:
        parseFloat(drupalSettings.wayfinding.location.bottomright.lng) *
        10000000,
    };
    const img_width = $(bgimg).width();
    const img_height = $(bgimg).height();

    // Calculate center.
    const m = {
      latitude: (topLeft.latitude + bottomRight.latitude) / 2,
      longitude: (topLeft.longitude + bottomRight.longitude) / 2,
    };

    // Calculate b.
    const b = {
      latitude: 2 * m.latitude - a.latitude,
      longitude: 2 * m.longitude - a.longitude,
    };

    // Calculate e1 and e2, f1 and f2.
    const e1 = Math.abs(p.latitude - a.latitude);
    const e2 = Math.abs(p.latitude - b.latitude);
    const f1 = Math.abs(p.longitude - a.longitude);
    const f2 = Math.abs(p.longitude - b.longitude);
    // Calculate g1 and g2.
    const g1 = Math.sqrt(e1 * e1 + f1 * f1);
    const g2 = Math.sqrt(e2 * e2 + f2 * f2);
    // Calculate alpha1 and alpha2.
    const alpha1 = (Math.asin(e1 / g1) * 180) / Math.PI;
    const alpha2 = (Math.asin(e2 / g2) * 180) / Math.PI;
    // Calculate m1 and m2, n1 and n2.
    const beta1 = Math.sin(((alpha1 + rotation) * Math.PI) / 180);
    const beta2 = Math.sin(((alpha2 + rotation) * Math.PI) / 180);
    const m1 = g1 * beta1;
    const m2 = g2 * beta2;
    const beta3 = Math.sin(((90 - alpha1 - rotation) * Math.PI) / 180);
    const beta4 = Math.sin(((90 - alpha2 - rotation) * Math.PI) / 180);
    const n1 = g1 * Math.sin(beta3);
    const n2 = g2 * Math.sin(beta4);
    const w = m1 + m2;
    const h = n1 + n2;

    const x = (m1 / w) * 100;
    const y = (n1 / h) * 100;
    $(pin).css('left', `${x}%`);
    $(pin).css('top', `${y}%`);
  };

  Drupal.wayfinding.setResetTimer = function (resetTimeout) {
    if (resetTimeout) {
      if (
        Drupal.digital_signage_timer &&
        Drupal.digital_signage_timer.popupTimer
      ) {
        Drupal.digital_signage_timer.resetTimeout(
          Drupal.digital_signage_timer.popupTimer,
          drupalSettings.wayfinding.resetTimeout,
        );
      } else if (window.parent) {
        try {
          window.parent.postMessage(
            {
              action: 'resetTimeout',
              timeout: drupalSettings.wayfinding.resetTimeout,
            },
            drupalSettings.wayfinding.origin,
          );
        } catch (err) {
          // It's ok if we can't reset the timeout.
        }
      }
    }
    if (Drupal.wayfinding.resetTimer) {
      window.clearTimeout(Drupal.wayfinding.resetTimer);
    }
    Drupal.wayfinding.resetTimer = window.setTimeout(function () {
      Drupal.wayfinding.popupClose();
      Drupal.wayfinding.reset();
    }, drupalSettings.wayfinding.resetTimeout * 1000);
  };

  Drupal.wayfinding.reset = function () {
    $('.legend .source').removeClass('active');
    $('.images .destinations .destination').removeClass('show');
    $('#wayfinding .background svg .wayfinding-show').removeClass(
      'wayfinding-show',
    );
    $('#wayfinding .widgets .widget.show').removeClass('show');
  };

  Drupal.wayfinding.popupClose = function () {
    $(
      '.popup-content-close.show,.popup-content-background.show,.popup-content.show',
    ).removeClass('show');
  };

  Drupal.wayfinding.popupSource = function (id) {
    $('.popup-content.show').removeClass('show');
    const content = $(`.popup-content.${id}`);
    if ($(content).length > 0) {
      $('.popup-content-background,.popup-content-close').addClass('show');
      $(content).addClass('show');
    }
  };
})(jQuery, Drupal, drupalSettings);
